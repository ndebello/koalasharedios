var timeBar = {
		offset: Math.PI/2,
		outerRadius:230,
		innerRadius:216,
		sleeping_hrs:8,
		getArcAngles: function(hrs, hrs2){
			return {'outer': 2* Math.PI/24 * hrs - this.offset, 'inner': 2* Math.PI/24 * hrs2 - this.offset };
		},
		getPathUnits: function(){
			return [0, 4, 8, 12, 16, 20, 22, 24 ];
		},
		drawUnits: function(){
			var canvas = document.getElementById('myCanvas');
			var context = canvas.getContext('2d');
			 var x = canvas.width / 2;
			var y = canvas.height / 2;
			 
			 var startAngle = 0 - this.offset;
			 var UNS = this.getPathUnits();
			  
			for(var i in UNS){
				i = parseInt(UNS[i]);
				context.lineWidth = 17;
				var counterClockwise = false;
				j = i+0.1;
				 var angles =  this.getArcAngles(i, 0);
				 var angles2 =  this.getArcAngles(j, 0);
				 console.log(angles, j, angles2);
				 context.beginPath(); 
				 context.arc(x, y, this.outerRadius, angles.outer, angles2.outer , counterClockwise);
				  // line color
				  context.strokeStyle = '#1CA4D9';
				  context.stroke();
			}
		},
		drawAll: function(active_hrs){
		  sleeping_hrs = this.sleeping_hrs;
		  var canvas = document.getElementById('myCanvas');
		  var context = canvas.getContext('2d');
		  var x = canvas.width / 2;
		  var y = canvas.height / 2;
		   
		  var startAngle = 0 - this.offset;
		  var endAngle =   Math.PI + this.offset;
		  
		  var angles =  this.getArcAngles(sleeping_hrs, active_hrs);
		  context.lineWidth = 17;
		  var counterClockwise = false;
			
			
			
		context.beginPath(); 
		  context.arc(x, y, this.outerRadius, startAngle, endAngle, counterClockwise);
		  // line color
		  context.strokeStyle = '#D2D6BF';
		  context.stroke();
		
		this.drawUnits();
		//return;
		  context.beginPath(); 
		  context.arc(x, y, this.outerRadius, startAngle, angles.outer, counterClockwise);
		  // line color
		  context.strokeStyle = '#1CA4D9';
		  context.stroke();
		  
		  context.lineWidth = 11;
		  
		   context.beginPath(); 
		  context.arc(x, y, this.innerRadius, startAngle, angles.inner, counterClockwise);
		  // line color
		  context.strokeStyle = '#009B00';
		  context.stroke();
		}
	};
	